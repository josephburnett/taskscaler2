package taskscaler

import (
	"fmt"
	"time"

	"github.com/hashicorp/go-hclog"

	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
)

type options struct {
	logger              hclog.Logger
	maxInstances        int
	capacityPerInstance int
	maxUseCount         int
	scheduleTimezone    string
	settings            provider.Settings
	acquireDelay        time.Duration

	upFn func(id string, info provider.ConnectInfo) error
}

// Option is an option used for configuring the autoscaler.
type Option func(*options) error

func newOptions(opts []Option) (options, error) {
	var options options

	options.logger = hclog.Default()
	options.acquireDelay = 2 * time.Second

	for _, o := range opts {
		err := o(&options)
		if err != nil {
			return options, err
		}
	}

	if options.capacityPerInstance <= 0 {
		options.capacityPerInstance = 1
	}

	return options, nil
}

// WithLogger settings the logger.
func WithLogger(logger hclog.Logger) Option {
	return func(o *options) error {
		o.logger = logger
		return nil
	}
}

// WithMaxInstances sets the maximum allowed instances.
func WithMaxInstances(n int) Option {
	return func(o *options) error {
		o.maxInstances = n
		return nil
	}
}

// WithCapacityPerInstance sets how many tasks can be executed concurrently per
// instance.
func WithCapacityPerInstance(n int) Option {
	return func(o *options) error {
		o.capacityPerInstance = n
		return nil
	}
}

// WithMaxUseCount sets the maximum number of times an instance can be assigned
// a task.
func WithMaxUseCount(n int) Option {
	return func(o *options) error {
		o.maxUseCount = n
		return nil
	}
}

// WithScheduleTimezone sets the schedule timezone.
func WithScheduleTimezone(tz string) Option {
	return func(o *options) error {
		_, err := time.LoadLocation(tz)
		if err != nil {
			return fmt.Errorf("schedule timezone: %w", err)
		}

		o.scheduleTimezone = tz
		return nil
	}
}

// WithInstanceUpFunc sets a callback function to be called when a new instance
// is ready to be connected to.
func WithInstanceUpFunc(fn func(id string, info provider.ConnectInfo) error) Option {
	return func(o *options) error {
		o.upFn = fn
		return nil
	}
}

// WithInstanceGroupSettings passes common instance group settings to the
// configured provider.
func WithInstanceGroupSettings(settings provider.Settings) Option {
	return func(o *options) error {
		o.settings = settings
		return nil
	}
}

// WithAcquireDelay configures the delay between checking to see if an instance
// asked for is ready.
func WithAcquireDelay(delay time.Duration) Option {
	return func(o *options) error {
		o.acquireDelay = delay
		return nil
	}
}
