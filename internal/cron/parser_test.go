package cron

import (
	"errors"
	"testing"
	"time"
)

func BenchmarkTaskscalerParse(b *testing.B) {
	b.ReportAllocs()

	pattern := "0-5,10-15,20-25,30-35,40-45,50-55 */3,*/4 1-15/3,16-31/4 * 2,WED,FRI,7"
	for i := 0; i < b.N; i++ {
		_, err := Parse(pattern, time.UTC.String())
		if err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkTaskscalerContains(b *testing.B) {
	periods, err := Parse("* * 29 feb *", time.UTC.String())
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		periods.Contains(time.Now())
	}
}

func assert(f field, input string, expectedErr error, expected ...Expr) func(t *testing.T) {
	return func(t *testing.T) {
		t.Helper()

		v, err := expression(f, input)
		if err != nil {
			if !errors.Is(err, expectedErr) {
				t.Errorf("expected error %v, got %v", expectedErr, err)
			}
		}
		if err == nil && expectedErr != nil {
			t.Errorf("expected error: %v, got nil", expectedErr)
		}

		if v == nil {
			if len(expected) == 0 {
				return
			}
			t.Errorf("expected %v, got %v", expected, v)
			return
		}

		many := v.(Many)
		if len(expected) != len(many) {
			t.Errorf("expected %v, got %v", expected, many)
			return
		}

		success := true
		for idx, e := range many {
			if expected[idx] != e {
				success = false
				break
			}
		}

		if !success {
			t.Errorf("expected %v, got %v", expected, many)
		}
	}
}

// Tests are based on:
// https://github.com/cloudflare/saffron/blob/bb34024be42c735a4f48e18b9eafc3aac455e9c6/saffron/src/parse.rs#L1115
// but modified for standard-cron, rather than quartz.
func TestMinutes(t *testing.T) {
	t.Run("all", assert(minuteExpr, "*", nil,
		All{}))

	t.Run("only_match_first_star", assert(minuteExpr, "*,*", ErrAnyAlreadyGiven,
		All{}))

	t.Run("star_step", assert(minuteExpr, "*/5", nil,
		Step{Range: All{}, Step: Value(5)}))

	t.Run("multi_star_step", assert(minuteExpr, "*/5,*/3", nil,
		Step{Range: All{}, Step: Value(5)},
		Step{Range: All{}, Step: Value(3)}))

	t.Run("star_range_doesnt_make_sense", assert(minuteExpr, "*-30/5,*/3", ErrInvalidExpression))

	t.Run("one_value", assert(minuteExpr, "0", nil,
		Value(0)))

	t.Run("many_one_value", assert(minuteExpr, "5,15,25,35,45,55", nil,
		Value(5), Value(15), Value(25), Value(35), Value(45), Value(55)))

	t.Run("one_range", assert(minuteExpr, "0-30", nil,
		Range{From: Value(0), To: Value(30)}))

	t.Run("overflow_range", assert(minuteExpr, "50-10", nil,
		Range{From: Value(50), To: Value(10)}))

	t.Run("many_range", assert(minuteExpr, "0-5,10-15,20-25,30-35,40-45,50-55", nil,
		Range{From: Value(0), To: Value(5)},
		Range{From: Value(10), To: Value(15)},
		Range{From: Value(20), To: Value(25)},
		Range{From: Value(30), To: Value(35)},
		Range{From: Value(40), To: Value(45)},
		Range{From: Value(50), To: Value(55)}))

	t.Run("step", assert(minuteExpr, "0/5", nil,
		Step{Range: Value(0), Step: Value(5)}))

	t.Run("step_with_star_step", assert(minuteExpr, "1/3,*/5", nil,
		Step{Range: Value(1), Step: Value(3)},
		Step{Range: All{}, Step: Value(5)}))

	t.Run("many_steps", assert(minuteExpr, "1/3,2/3,5/10", nil,
		Step{Range: Value(1), Step: Value(3)},
		Step{Range: Value(2), Step: Value(3)},
		Step{Range: Value(5), Step: Value(10)}))

	t.Run("range_step", assert(minuteExpr, "0-30/5", nil,
		Step{Range: Range{From: Value(0), To: Value(30)}, Step: Value(5)}))

	t.Run("many_range_step", assert(minuteExpr, "0-30/5,30-59/3", nil,
		Step{Range: Range{From: Value(0), To: Value(30)}, Step: Value(5)},
		Step{Range: Range{From: Value(30), To: Value(59)}, Step: Value(3)}))

	t.Run("values_ranges_steps_and_ranges", assert(minuteExpr, "0,5-10,10-30/3,30/3", nil,
		Value(0),
		Range{From: Value(5), To: Value(10)},
		Step{Range: Range{From: Value(10), To: Value(30)}, Step: Value(3)},
		Step{Range: Value(30), Step: Value(3)}))

	t.Run("limit_max", assert(minuteExpr, "60", ErrExceedsMaximum))
	t.Run("limit_max_range", assert(minuteExpr, "0-60", ErrExceedsMaximum))
	t.Run("limit_max_step", assert(minuteExpr, "0/60", ErrExceedsMaximum))
	t.Run("limit_max_step_range", assert(minuteExpr, "0-60/5", ErrExceedsMaximum))
	t.Run("limit_max_step_zero", assert(minuteExpr, "0/0", ErrInvalidStepZero))
	t.Run("limit_max_step_zero", assert(minuteExpr, "0-59/0", ErrInvalidStepZero))
}

func TestHours(t *testing.T) {
	t.Run("all", assert(hourExpr, "*", nil,
		All{}))

	t.Run("only_match_first_star", assert(hourExpr, "*,*", ErrAnyAlreadyGiven,
		All{}))

	t.Run("star_step", assert(hourExpr, "*/3", nil,
		Step{Range: All{}, Step: Value(3)}))

	t.Run("multi_star_step", assert(hourExpr, "*/3,*/4", nil,
		Step{Range: All{}, Step: Value(3)},
		Step{Range: All{}, Step: Value(4)}))

	t.Run("star_range_doesnt_make_sense", assert(hourExpr, "*-6/3,*/4", ErrInvalidExpression))

	t.Run("one_value", assert(hourExpr, "0", nil,
		Value(0)))

	t.Run("many_one_value", assert(hourExpr, "0,3,6,9,12,15,18,21", nil,
		Value(0), Value(3), Value(6), Value(9), Value(12), Value(15), Value(18), Value(21)))

	t.Run("one_range", assert(hourExpr, "0-12", nil,
		Range{From: Value(0), To: Value(12)}))

	t.Run("overflow_range", assert(hourExpr, "22-2", nil,
		Range{From: Value(22), To: Value(2)}))

	t.Run("many_range", assert(hourExpr, "0-3,6-9,12-15,18-21", nil,
		Range{From: Value(0), To: Value(3)},
		Range{From: Value(6), To: Value(9)},
		Range{From: Value(12), To: Value(15)},
		Range{From: Value(18), To: Value(21)}))

	t.Run("step", assert(hourExpr, "0/3", nil,
		Step{Range: Value(0), Step: Value(3)}))

	t.Run("step_with_star_step", assert(hourExpr, "1/2,*/4", nil,
		Step{Range: Value(1), Step: Value(2)},
		Step{Range: All{}, Step: Value(4)}))

	t.Run("many_steps", assert(hourExpr, "1/2,2/3,3/4", nil,
		Step{Range: Value(1), Step: Value(2)},
		Step{Range: Value(2), Step: Value(3)},
		Step{Range: Value(3), Step: Value(4)}))

	t.Run("range_step", assert(hourExpr, "0-12/4", nil,
		Step{Range: Range{From: Value(0), To: Value(12)}, Step: Value(4)}))

	t.Run("many_range_step", assert(hourExpr, "0-12/4,12-23/3", nil,
		Step{Range: Range{From: Value(0), To: Value(12)}, Step: Value(4)},
		Step{Range: Range{From: Value(12), To: Value(23)}, Step: Value(3)}))

	t.Run("values_ranges_steps_and_ranges", assert(hourExpr, "0,0-6/3,6-12,12/3", nil,
		Value(0),
		Step{Range: Range{From: Value(0), To: Value(6)}, Step: Value(3)},
		Range{From: Value(6), To: Value(12)},
		Step{Range: Value(12), Step: Value(3)}))

	t.Run("limit_max", assert(hourExpr, "24", ErrExceedsMaximum))
	t.Run("limit_max_range", assert(hourExpr, "0-24", ErrExceedsMaximum))
	t.Run("limit_max_step", assert(hourExpr, "0/24", ErrExceedsMaximum))
	t.Run("limit_max_step_range", assert(hourExpr, "0-24/5", ErrExceedsMaximum))
	t.Run("limit_max_step_zero", assert(hourExpr, "0/0", ErrInvalidStepZero))
	t.Run("limit_max_step_zero", assert(hourExpr, "0-23/0", ErrInvalidStepZero))
}

func TestMonths(t *testing.T) {
	t.Run("all", assert(monthExpr, "*", nil,
		All{}))

	t.Run("only_match_first_star", assert(monthExpr, "*,*", ErrAnyAlreadyGiven,
		All{}))

	t.Run("star_step", assert(monthExpr, "*/3", nil,
		Step{Range: All{}, Step: Value(3)}))

	t.Run("multi_star_step", assert(monthExpr, "*/3,*/4", nil,
		Step{Range: All{}, Step: Value(3)},
		Step{Range: All{}, Step: Value(4)}))

	t.Run("star_range_doesnt_make_sense", assert(monthExpr, "*-6/3,*/4", ErrInvalidExpression))

	t.Run("one_value", assert(monthExpr, "1", nil,
		Value(1)))

	t.Run("word_values", func(t *testing.T) {
		t.Run("", assert(monthExpr, "JAN", nil, Value(1)))
		t.Run("", assert(monthExpr, "FEB", nil, Value(2)))
		t.Run("", assert(monthExpr, "MAR", nil, Value(3)))
		t.Run("", assert(monthExpr, "APR", nil, Value(4)))

		t.Run("", assert(monthExpr, "may", nil, Value(5)))
		t.Run("", assert(monthExpr, "jun", nil, Value(6)))
		t.Run("", assert(monthExpr, "jul", nil, Value(7)))
		t.Run("", assert(monthExpr, "aug", nil, Value(8)))

		t.Run("", assert(monthExpr, "sEp", nil, Value(9)))
		t.Run("", assert(monthExpr, "ocT", nil, Value(10)))
		t.Run("", assert(monthExpr, "NOv", nil, Value(11)))
		t.Run("", assert(monthExpr, "Dec", nil, Value(12)))
	})

	t.Run("many_one_value", assert(monthExpr, "1,MAR,6,SEP,12", nil,
		Value(1), Value(3), Value(6), Value(9), Value(12)))

	t.Run("one_range", assert(monthExpr, "1-12", nil,
		Range{From: Value(1), To: Value(12)}))

	t.Run("one_range_words", assert(monthExpr, "JAN-DEC", nil,
		Range{From: Value(1), To: Value(12)}))

	t.Run("overflow_range_number_words", assert(monthExpr, "11-FEB", nil,
		Range{From: Value(11), To: Value(2)}))

	t.Run("overflow_range_words_number", assert(monthExpr, "NOV-2", nil,
		Range{From: Value(11), To: Value(2)}))

	t.Run("many_range", assert(monthExpr, "1-MAR,MAY-7,SEP-11", nil,
		Range{From: Value(1), To: Value(3)},
		Range{From: Value(5), To: Value(7)},
		Range{From: Value(9), To: Value(11)}))

	t.Run("step", assert(monthExpr, "1/3", nil,
		Step{Range: Value(1), Step: Value(3)}))

	t.Run("step_with_star_step", assert(monthExpr, "2/2,*/4", nil,
		Step{Range: Value(2), Step: Value(2)},
		Step{Range: All{}, Step: Value(4)}))

	t.Run("many_steps", assert(monthExpr, "1/2,FEB/3,3/4", nil,
		Step{Range: Value(1), Step: Value(2)},
		Step{Range: Value(2), Step: Value(3)},
		Step{Range: Value(3), Step: Value(4)}))

	t.Run("range_step", assert(monthExpr, "1-DEC/4", nil,
		Step{Range: Range{From: Value(1), To: Value(12)}, Step: Value(4)}))

	t.Run("many_range_step", assert(monthExpr, "1-JUN/4,JUN-12/3", nil,
		Step{Range: Range{From: Value(1), To: Value(6)}, Step: Value(4)},
		Step{Range: Range{From: Value(6), To: Value(12)}, Step: Value(3)}))

	t.Run("values_ranges_steps_and_ranges", assert(monthExpr, "1,JAN-6/3,JUN-12,DEC/3", nil,
		Value(1),
		Step{Range: Range{From: Value(1), To: Value(6)}, Step: Value(3)},
		Range{From: Value(6), To: Value(12)},
		Step{Range: Value(12), Step: Value(3)}))

	t.Run("limit_min", assert(monthExpr, "0", ErrExceedsMinimum))
	t.Run("limit_max", assert(monthExpr, "13", ErrExceedsMaximum))
	t.Run("limit_min_range", assert(monthExpr, "0-12", ErrExceedsMinimum))
	t.Run("limit_max_range", assert(monthExpr, "1-13", ErrExceedsMaximum))
	t.Run("limit_min_step", assert(monthExpr, "0/12", ErrExceedsMinimum))
	t.Run("limit_max_step", assert(monthExpr, "1/13", ErrExceedsMaximum))
	t.Run("limit_min_step_range", assert(monthExpr, "0-12/2", ErrExceedsMinimum))
	t.Run("limit_max_step_range", assert(monthExpr, "1-13/2", ErrExceedsMaximum))
	t.Run("limit_max_step_zero", assert(monthExpr, "1/0", ErrInvalidStepZero))
	t.Run("limit_max_step_zero", assert(monthExpr, "1-12/0", ErrInvalidStepZero))
}

func TestDaysOfMonth(t *testing.T) {
	t.Run("all", assert(domExpr, "*", nil,
		All{}))

	t.Run("only_match_first_star", assert(domExpr, "*,*", ErrAnyAlreadyGiven,
		All{}))

	t.Run("star_step", assert(domExpr, "*/3", nil,
		Step{Range: All{}, Step: Value(3)}))

	t.Run("multi_star_step", assert(domExpr, "*/3,*/4", nil,
		Step{Range: All{}, Step: Value(3)},
		Step{Range: All{}, Step: Value(4)}))

	t.Run("star_range_doesnt_make_sense", assert(domExpr, "*-6/3,*/4", ErrInvalidExpression))

	t.Run("one_value", assert(domExpr, "1", nil,
		Value(1)))

	t.Run("many_one_value", assert(domExpr, "1,4,7,10,13,16,19,22,25,28,31", nil,
		Value(1), Value(4), Value(7), Value(10), Value(13), Value(16), Value(19), Value(22), Value(25), Value(28), Value(31)))

	t.Run("one_range", assert(domExpr, "1-15", nil,
		Range{From: Value(1), To: Value(15)}))

	t.Run("overflow_range", assert(domExpr, "30-1", nil,
		Range{From: Value(30), To: Value(1)}))

	t.Run("many_range", assert(domExpr, "1-4,5-8,9-12,13-15", nil,
		Range{From: Value(1), To: Value(4)},
		Range{From: Value(5), To: Value(8)},
		Range{From: Value(9), To: Value(12)},
		Range{From: Value(13), To: Value(15)}))

	t.Run("step", assert(domExpr, "1/3", nil,
		Step{Range: Value(1), Step: Value(3)}))

	t.Run("step_with_star_step", assert(domExpr, "2/2,*/4", nil,
		Step{Range: Value(2), Step: Value(2)},
		Step{Range: All{}, Step: Value(4)}))

	t.Run("many_steps", assert(domExpr, "1/2,2/3,3/4", nil,
		Step{Range: Value(1), Step: Value(2)},
		Step{Range: Value(2), Step: Value(3)},
		Step{Range: Value(3), Step: Value(4)}))

	t.Run("range_step", assert(domExpr, "1-15/4", nil,
		Step{Range: Range{From: Value(1), To: Value(15)}, Step: Value(4)}))

	t.Run("many_range_step", assert(domExpr, "1-15/3,16-31/4", nil,
		Step{Range: Range{From: Value(1), To: Value(15)}, Step: Value(3)},
		Step{Range: Range{From: Value(16), To: Value(31)}, Step: Value(4)}))

	t.Run("values_ranges_steps_and_ranges", assert(domExpr, "1,1-10/3,10-20,20/3", nil,
		Value(1),
		Step{Range: Range{From: Value(1), To: Value(10)}, Step: Value(3)},
		Range{From: Value(10), To: Value(20)},
		Step{Range: Value(20), Step: Value(3)}))

	t.Run("limit_min", assert(domExpr, "0", ErrExceedsMinimum))
	t.Run("limit_max", assert(domExpr, "32", ErrExceedsMaximum))
	t.Run("limit_min_range", assert(domExpr, "0-31", ErrExceedsMinimum))
	t.Run("limit_max_range", assert(domExpr, "1-32", ErrExceedsMaximum))
	t.Run("limit_min_step", assert(domExpr, "0/31", ErrExceedsMinimum))
	t.Run("limit_max_step", assert(domExpr, "1/32", ErrExceedsMaximum))
	t.Run("limit_min_step_range", assert(domExpr, "0-31/31", ErrExceedsMinimum))
	t.Run("limit_max_step_range", assert(domExpr, "1-32/32", ErrExceedsMaximum))
	t.Run("limit_max_step_range", assert(domExpr, "0-32/32", ErrExceedsMaximum))
	t.Run("limit_max_step_zero", assert(domExpr, "1/0", ErrInvalidStepZero))
	t.Run("limit_max_step_zero", assert(domExpr, "1-23/0", ErrInvalidStepZero))
}

func TestDaysOfWeek(t *testing.T) {
	t.Run("all", assert(dowExpr, "*", nil,
		All{}))

	t.Run("only_match_first_star", assert(dowExpr, "*,*", ErrAnyAlreadyGiven,
		All{}))

	t.Run("star_step", assert(dowExpr, "*/3", nil,
		Step{Range: All{}, Step: Value(3)}))

	t.Run("multi_star_step", assert(dowExpr, "*/3,*/4", nil,
		Step{Range: All{}, Step: Value(3)},
		Step{Range: All{}, Step: Value(4)}))

	t.Run("star_range_doesnt_make_sense", assert(dowExpr, "*-6/3,*/4", ErrInvalidExpression))

	t.Run("one_value", assert(dowExpr, "1", nil,
		Value(1)))

	t.Run("word_values", func(t *testing.T) {
		t.Run("", assert(dowExpr, "SUN", nil, Value(0)))
		t.Run("", assert(dowExpr, "MON", nil, Value(1)))
		t.Run("", assert(dowExpr, "TUE", nil, Value(2)))

		t.Run("", assert(dowExpr, "wed", nil, Value(3)))
		t.Run("", assert(dowExpr, "thu", nil, Value(4)))
		t.Run("", assert(dowExpr, "fri", nil, Value(5)))

		t.Run("", assert(dowExpr, "SaT", nil, Value(6)))
	})

	t.Run("many_one_value", assert(dowExpr, "2,WED,FRI,7", nil,
		Value(2), Value(3), Value(5), Value(7)))

	t.Run("one_range", assert(dowExpr, "2-5", nil,
		Range{From: Value(2), To: Value(5)}))

	t.Run("overflow_range", assert(dowExpr, "7-1", nil,
		Range{From: Value(7), To: Value(1)}))

	t.Run("many_range", assert(dowExpr, "1-3,4-4,5-7", nil,
		Range{From: Value(1), To: Value(3)},
		Range{From: Value(4), To: Value(4)},
		Range{From: Value(5), To: Value(7)}))

	t.Run("step", assert(dowExpr, "2/2", nil,
		Step{Range: Value(2), Step: Value(2)}))

	t.Run("step_with_star_step", assert(dowExpr, "2/2,*/4", nil,
		Step{Range: Value(2), Step: Value(2)},
		Step{Range: All{}, Step: Value(4)}))

	t.Run("many_steps", assert(dowExpr, "1/2,2/3,3/4", nil,
		Step{Range: Value(1), Step: Value(2)},
		Step{Range: Value(2), Step: Value(3)},
		Step{Range: Value(3), Step: Value(4)}))

	t.Run("range_step", assert(dowExpr, "2-5/2", nil,
		Step{Range: Range{From: Value(2), To: Value(5)}, Step: Value(2)}))

	t.Run("many_range_step", assert(dowExpr, "1-4/2,5-7/2", nil,
		Step{Range: Range{From: Value(1), To: Value(4)}, Step: Value(2)},
		Step{Range: Range{From: Value(5), To: Value(7)}, Step: Value(2)}))

	t.Run("values_ranges_steps_and_ranges", assert(dowExpr, "1,2-FRI/2,6-7,3/3", nil,
		Value(1),
		Step{Range: Range{From: Value(2), To: Value(5)}, Step: Value(2)},
		Range{From: Value(6), To: Value(7)},
		Step{Range: Value(3), Step: Value(3)}))

	t.Run("limit_max", assert(dowExpr, "8", ErrExceedsMaximum))
	t.Run("limit_max_range", assert(dowExpr, "0-8", ErrExceedsMaximum))
	t.Run("limit_max_step", assert(dowExpr, "0/8", ErrExceedsMaximum))
	t.Run("limit_max_step_range", assert(dowExpr, "0-8/5", ErrExceedsMaximum))
	t.Run("limit_max_step_zero", assert(dowExpr, "0/0", ErrInvalidStepZero))
	t.Run("limit_max_step_zero", assert(dowExpr, "0-8/0", ErrInvalidStepZero))
}
