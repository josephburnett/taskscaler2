package capacity

import (
	"math"
)

type CapacityInfo struct {
	// InstanceCount is active instances: creating, running, requested
	InstanceCount int

	// MaxInstanceCount is the maximum number of instances supported by the
	// instance group.
	MaxInstanceCount int

	// Acquired is instance capacity that has been acquired
	Acquired int

	// UnavailableCapacity is instance capacity that is unable to be used, this
	// is typically due to an instance having a max number of uses.
	// For example, an instance with a capacity of 5, but only 4 more uses,
	// effectively only has a capacity of 4.
	UnavailableCapacity int

	// Pending is pending tasks that do not yet have an acquisition
	Pending int

	// IdleCount is the desired idle capacity we want to maintain
	IdleCount int

	// ScaleFactor is used for calculating additional idle capacity based on
	// demand
	ScaleFactor float64

	// ScaleFactorLimit limits additional idle capacity
	ScaleFactorLimit int

	// CapacityPerInstance is how much capacity an instance provides
	CapacityPerInstance int
}

// RequiredInstance returns how many instances should be added or removed, based
// upon existing capacity and demand. It returns 0 if no change is required.
func RequiredInstances(info CapacityInfo) int {
	// the pending capacity might be wholly satisfied by the idle count, so
	// the demand for new capacity is the larger value between the idle
	// count we wish to maintain and the pending capacity.
	demand := max(info.IdleCount, info.Pending)

	// the desired capacity is our existing acquired capacity plus demand
	desired := info.Acquired + demand

	// use (acquired*scale factor) if it's greater than our existing desired
	// capacity
	if info.ScaleFactor > 0 {
		scale := int(math.Ceil(info.ScaleFactor * float64(info.Acquired)))
		if info.ScaleFactorLimit > 0 {
			scale = min(scale, info.ScaleFactorLimit)
		}
		desired = max(desired, scale)
	}

	// We treat unavailable capacity as capacity that will forever be "acquired"
	desired += info.UnavailableCapacity

	// required is how many instances are required in order to satisfy the
	// desired capacity it is desired divided by the capacity per instance
	required := int(math.Ceil(float64(desired) / float64(info.CapacityPerInstance)))

	if info.MaxInstanceCount > 0 {
		required = min(required, info.MaxInstanceCount)
	}

	// the required capacity is modified based upon how many instances are
	// already in-flight (requesting + creating) or are currently processing the
	// acquired demand.
	return required - info.InstanceCount
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
